# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.1] - 2023-07-20

### Changed

- Corrected icon used in disconnect message

## [2.0.0] - 2023-07-20

### Changed

- Changed options to provide short and long versions
- Renamed several options
- Set default location as variable in conf file
- Changed location script arguments to ISO 3166-1 alpha-2 codes, optionally by an abbreviation for the city in case a country has multiple servers

## [1.4.0] - 2023-07-20

### Changed

- Added completions

## [1.3.2] - 2023-06-09

### Changed

- Added new servers
- Formatting of conf file

## [1.3.1] - 2023-02-11

### Changed

- Updated server codes to match AzireVPN's configuration script
- Added new servers

## [1.3.0] - 2021-12-10

### Changed

- Refactored code to simplify maintenance

## [1.2.0] - 2021-12-09

### Added

- Consider execution status of Wireguard's wg-quick command

## [1.1.0] - 2021-12-09

### Changed

- Replaced `ifconfig` with `ip link` to retrieve current location
- Refactored retrieval of current location into a function

## [1.0.0] - 2021-12-07

Initial release.
