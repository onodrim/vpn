set --global vpn_version 2.0.1

set --global _vpn_default_location nl

set _vpn_code_prefix azirevpn

# Server information as an improvised dictionary with two values for each key
# 'AzireVPN server code' 'script argument' 'location string'
set --global _vpn_azirevpn_locations \
    "$_vpn_code_prefix-ca-tor" ca "Canada (Toronto)" \
    "$_vpn_code_prefix-ch-zrh" ch "Switzerland (Zurich)" \
    "$_vpn_code_prefix-de-ber" de-ber "Germany (Berlin)" \
    "$_vpn_code_prefix-de-fra" de-fra "Germany (Frankfurt)" \
    "$_vpn_code_prefix-dk-denmark" dk "Denmark (Copenhagen)" \
    "$_vpn_code_prefix-es-mad" es-mad "Spain (Madrid)" \
    "$_vpn_code_prefix-es-mal" es-mal "Spain (Málaga)" \
    "$_vpn_code_prefix-fi-hel" fi "Finland (Helsinki)" \
    "$_vpn_code_prefix-fr-par" fr "France (Paris)" \
    "$_vpn_code_prefix-gb-lon" uk "UK (London)" \
    "$_vpn_code_prefix-hk-hkg" hk "Hong Kong (Hong Kong)" \
    "$_vpn_code_prefix-it-mil" it "Italy (Milan)" \
    "$_vpn_code_prefix-nl-ams" nl "Netherlands (Amsterdam)" \
    "$_vpn_code_prefix-no-osl" no "Norway (Oslo)" \
    "$_vpn_code_prefix-ro-buh" ro "Romania (Bucharest)" \
    "$_vpn_code_prefix-se-sto" se-sto "Sweden (Stockholm)" \
    "$_vpn_code_prefix-se-got" se-got "Sweden (Gothenburg)" \
    "$_vpn_code_prefix-sg-sin" sg "Singapore (Singapore)" \
    "$_vpn_code_prefix-th-hkt" th "Thailand (Rawai)" \
    "$_vpn_code_prefix-us-chi" us-chi "USA (Chicago)" \
    "$_vpn_code_prefix-us-dal" us-dal "USA (Dalas)" \
    "$_vpn_code_prefix-us-lax" us-lax "USA (Los Angeles)" \
    "$_vpn_code_prefix-us-mia" us-mia "USA (Miami)" \
    "$_vpn_code_prefix-us-nyc" us-nyc "USA (New York)" \
    "$_vpn_code_prefix-us-sea" us-sea "USA (Seatle)"

# Modified from https://stackoverflow.com/a/69865337
# arg is the string to search for in locations
# init is the index where search should be started
# inc is an integer used to get the index of the element to return if a match for arg is found
function _vpn_extract_from_locations --argument-names arg init inc
    set -l keyseq (seq $init 3 (count $_vpn_azirevpn_locations))
    # we can't simply use `contains` because it won't distinguish keys from values
    for idx in $keyseq
        if test $arg = $_vpn_azirevpn_locations[$idx]
            echo $_vpn_azirevpn_locations[(math $idx + $inc)]
            return
        end
    end
    return 1
end

function _vpn_code_to_string --argument-names code
    _vpn_extract_from_locations $code 1 2
end

function _vpn_argument_to_code --argument-names arg
    _vpn_extract_from_locations $arg 2 -1
end
