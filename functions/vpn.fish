function _vpn_print_help
    printf %s\n \
        " Usage: vpn [options]" \
        " Options:" \
        "  -h or --help                     Display this help message." \
        "  -v or --version                  Display vpn version number" \
        "  -l or --list                     List supported locations and location codes." \
        "  -s or --status                   Display the current connection status." \
        "  -u[location] or --up[=location]  Turn on VPN and connect to default/specified location." \
        "  -d or --down                     Turn VPN off."
end

function _vpn_print_locations
    echo ' Currently supported locations and their corresponding argument:'
    echo

    set output ' Location===Argument\n'
    set -a output '----------------------===-----------\n'
    set -l keyseq (seq 1 3 (count $_vpn_azirevpn_locations))
    for idx in $keyseq
        set -l loc_string $_vpn_azirevpn_locations[(math $idx + 2)]
        set -l loc_arg $_vpn_azirevpn_locations[(math $idx + 1)]
        set -a output "$loc_string===$loc_arg\n"
    end
    echo -e "$output" | column --table --separator ===
end

function _vpn_current_location
    set -l loc (ip link | string match -r 'azirevpn-(?:\w|-)+')
    test -n "$loc"; or return
    echo $loc
end

function _vpn_status
    if set -l current_location (_vpn_current_location)
        echo -e " ✅ You are connected to AzireVPN in "(_vpn_code_to_string $current_location)" via WireGuard."
        return 0
    else
        echo -e " ❌ You are not connected to AzireVPN via WireGuard."
        return 1
    end
end

function _vpn_connect --argument-names location
    if test -z "$location"
        set location $_vpn_default_location
    end

    set -l code (_vpn_argument_to_code $location)
    if test -n "$code"
        if set -l current_location (_vpn_current_location)
            echo -e " ✅ You are already connected to AzireVPN in "(_vpn_code_to_string $current_location)" via WireGuard."
            _vpn_disconnect
        end

        echo -e " 📡 Connecting to AzireVPN in "(_vpn_code_to_string $code)"..."
        _vpn_wireguard_action up $code
    else
        echo -e " ❌ Incorrect location argument \"$location\", use \"vpn -l\" to list all valid arguments."
        return 121
    end
end

function _vpn_disconnect
    if set -l current_location (_vpn_current_location)
        echo -e " 󱠡  Disconnecting from AzireVPN in "(_vpn_code_to_string $current_location)"..."
        _vpn_wireguard_action down $current_location
    else
        echo -e " ❌ You are not connected to AzireVPN via WireGuard."
        return 1
    end
end

function _vpn_wireguard_action --argument-names action server
    if command wg-quick $action $server >/dev/null 2>&1
        echo " ✅ Done."
        return 0
    else
        echo " ❌ An error occurred while executing the WireGuard command."
        return 1
    end
end

function vpn --description "Manage WireGuard connections for AzireVPN"
    argparse v/version h/help l/list s/status 'u/up=?' d/down -- $argv

    if set -q _flag_version
        echo "vpn, version $vpn_version"
    else if set -q _flag_help
        _vpn_print_help
    else if set -q _flag_list
        _vpn_print_locations
    else if set -q _flag_status
        _vpn_status
    else if set -q _flag_up
        _vpn_connect $_flag_up
    else if set -q _flag_down
        _vpn_disconnect
    else
        _vpn_print_help
        return 1
    end
end

# Local Variables:
# jinx-local-words: "AzireVPN WireGuard arg azirevpn"
# End:
